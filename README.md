# Remote Development Team Automation

Automation tools related to the
[GitLab Remote Development Team's process](https://handbook.gitlab.com/handbook/engineering/development/dev/create/remote-development/#-planning-process).

This repo is primarily intended for MVS/spike early iteration on process automation.

The processes/tools/scripts in this repo may eventually be migrated to other more permanent locations, such as the Triage Bot project.

# Automations

- [Workspaces Iteration Planning Report](https://gitlab-org.gitlab.io/remote-development/remote-development-team-automation/remote-development-iteration-planning-report-latest.html)

# RBS Type Checking

- Run following to check types (first two only need to be run once):
    - bin/rbs collection install
    - bin/rbs collection clean
    - bin/steep check
- Update `sig/automation/automation.rbs` to reflect any type signature changes

# spec/planning_report_spec.rb
require "rspec"
require "open3"

# noinspection RbsMissingTypeSignature
describe "planning_report.rb script E2E integration" do
  shared_examples "a successful execution" do
    attr_reader :stdout, :stderr, :status

    before(:all) do
      @report_execution = Open3.capture3("bundle exec #{__dir__}/../scripts/#{@report_script}")
      @stdout = @report_execution[0]
      @stderr = @report_execution[1]
      @status = @report_execution[2]
    end

    it "is successful" do
      if status.exitstatus != 0
        puts "Command failed!"
        puts "stdout: #{stdout}"
        puts "stderr: #{stderr}"
      end

      expect(status).to be_success
    end

    it "is has no error output" do
      expect(stderr).to be_empty
    end

    it "outputs the top-level header" do
      expect(stdout).to include("# #{category_name} Velocity-Based Iteration Planning Report")
    end

    it "outputs the top-level header" do
      expect(stdout).to include("# #{category_name} Velocity-Based Iteration Planning Report")
    end

    it "contains past iteration output" do
      pattern = %r{\| \[\d{4}-\d{2}-\d{2} - \d{4}-\d{2}-\d{2}\]\(https://gitlab.com/groups/gitlab-org/-/iterations/\d+\) \| \d+ \| \d+ \|}
      expect(stdout).to match(pattern)
    end

    it "contains current/future iteration output" do
      pattern = %r{### \d{4}-\d{2}-\d{2} - \d{4}-\d{2}-\d{2}: \d+ points, \d+ issues}
      expect(stdout).to match(pattern)
    end
  end

  context "for Workspaces category" do
    let(:category_name) { "Workspaces" }

    before(:all) do
      @report_script = "workspaces-planning-report.rb"
    end

    it_should_behave_like "a successful execution"
  end

  context "for Workspaces category" do
    let(:category_name) { "Web IDE" }

    before(:all) do
      @report_script = "web-ide-planning-report.rb"
    end

    it_should_behave_like "a successful execution"
  end
end

#!/usr/bin/env ruby
# frozen_string_literal: true

# Prints a velocity-based planning report for a team's iteration cadence
# One-liner with pandoc (`brew install pandoc`)
# rubocop:disable Layout/LineLength
# scripts/agile_report/workspaces-planning-report.rb | pandoc -f markdown --metadata pagetitle='Agile Planning Report' -t html -s -o '/tmp/agile-planning-report.html' && open '/tmp/agile-planning-report.html'
# rubocop:enable Layout/LineLength
require_relative "../lib/automation/planning_report"
ENV["CATEGORY_NAME"] = "Web IDE"
ENV["ITERATIONS_CADENCE_GID"] = "gid://gitlab/Iterations::Cadence/1047922"
ENV["ITERATION_PLANNING_BOARD_GID"] = "gid://gitlab/Board/7440987"

planning_report = Automation::PlanningReport.new
planning_report.generate

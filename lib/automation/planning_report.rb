# frozen_string_literal: true

require "active_support/core_ext/hash/keys"
require "graphql/client"
require "graphql/client/http"
require_relative "graphql_api_facade"
require_relative "report_writer"

# Prints a velocity-based planning report for a team's iteration cadence
# Invoke via scripts/agile_report/workspaces-planning-report.rb
module Automation
  # A class to generate a velocity-based planning report for a team's iteration cadence
  class PlanningReport
    # noinspection MissingYardReturnTag
    def generate
      ################################################
      # Settings which must be configured via ENV vars:
      # API_TOKEN (read in GraphqlApiFacade))
      # CATEGORY_NAME (e.g. "Workspaces" or "Web IDE")
      # ITERATIONS_CADENCE_ID (e.g. "gid://gitlab/Iterations::Cadence/28324")
      # ITERATION_PLANNING_BOARD_GID (e.g. "gid://gitlab/Board/5283620")
      category_name = ENV.fetch("CATEGORY_NAME")
      # In the following line add dashes to the category_name.downcase
      report_name_prefix = ENV.fetch("REPORT_NAME_PREFIX", "#{category_name.downcase.split.join("-")}-iteration-planning-report")
      num_iterations_to_average = ENV.fetch("NUM_ITERATIONS_TO_AVERAGE", "4").to_i
      group_full_path = ENV.fetch("GROUP_FULL_PATH", "gitlab-org")
      iterations_cadence_gid = ENV.fetch("ITERATIONS_CADENCE_GID")
      iteration_planning_board_gid = ENV.fetch("ITERATION_PLANNING_BOARD_GID")
      board_labels = ENV.fetch("LABELS", "Category:#{category_name}")
      debug = %w[true 1].include?(ENV.fetch("DEBUG", "false"))
      ################################################

      data_from_graphql_api = get_data_from_graphql_api(
        group_full_path:,
        iterations_cadence_gid:,
        iteration_planning_board_gid:,
        num_iterations_to_average:,
        board_labels:,
        category_name:,
        debug:
      )

      prioritized_issues = order_issues_by_relative_position(data_from_graphql_api.fetch(:issues_for_prioritized))
      unprioritized_issues = order_issues_by_relative_position(data_from_graphql_api.fetch(:issues_for_unprioritized))

      prepend_closed_issues_for_current_iteration_to_prioritized(data_from_graphql_api:, prioritized_issues:)

      report_data = {
        board_web_url: data_from_graphql_api.fetch(:board_web_url),
        closed_iterations: data_from_graphql_api.fetch(:closed_iterations),
        prioritized_issues:,
        unprioritized_issues:,
        current_iteration: data_from_graphql_api.fetch(:current_iteration),
        category_name:,
        report_name_prefix:
      }

      ReportWriter.new.write_report(report_data:)
    end

    private

    def order_issues_by_relative_position(issues)
      # relativePosition is the position within a SINGLE board list (NOT the entire board!)
      issues.sort_by { |issue| issue.fetch(:relativePosition) }
    end

    def prepend_closed_issues_for_current_iteration_to_prioritized(data_from_graphql_api:, prioritized_issues:)
      # Add closed issues for current iteration, because closed issues are not shown in a label list.
      prioritized_issues.unshift(*data_from_graphql_api.fetch(:closed_issues_for_current_iteration))
    end

    # noinspection MissingYardParamTag
    # @return [Hash]
    def get_data_from_graphql_api(
      group_full_path:,
      iterations_cadence_gid:,
      iteration_planning_board_gid:,
      num_iterations_to_average:,
      board_labels:,
      category_name:,
      debug:
    )
      category_label_prefix = category_name.downcase.split.join
      api = GraphqlApiFacade.new

      closed_iterations = api.iterations(
        group_full_path:,
        iterations_cadence_gid:,
        first: num_iterations_to_average,
        state: "closed"
      )

      puts "\n\n\nCLOSED ITERATIONS:" if debug
      pp closed_iterations if debug

      board = api.lists_for_board(
        group_full_path:,
        iteration_planning_board_gid:
      )
      # noinspection RubyResolve
      board_web_url = board.web_url
      # noinspection RubyResolve
      lists_for_board = board.lists.nodes.map(&:to_h)

      unprioritized_list = lists_for_board.detect { |list| list.fetch("title") == "#{category_label_prefix}-workflow::unprioritized" }

      unprioritized_list_gid = unprioritized_list.fetch("id")

      puts "\n\n\nUNPRIORITIZED LIST:" if debug
      pp unprioritized_list if debug

      issues_for_unprioritized = api.issues_for_board_list(
        board_list_gid: unprioritized_list_gid,
        label_names: board_labels.split(",")
      )

      puts "\n\n\nISSUES FOR UNPRIORITIZED:" if debug
      pp issues_for_unprioritized if debug

      prioritized_list = lists_for_board.detect { |list| list.fetch("title") == "#{category_label_prefix}-workflow::prioritized" }
      prioritized_list_gid = prioritized_list.fetch("id")

      puts "\n\n\nPRIORITIZED LIST:" if debug
      pp prioritized_list if debug

      issues_for_prioritized = api.issues_for_board_list(
        board_list_gid: prioritized_list_gid,
        label_names: board_labels.split(",")
      )

      puts "\n\n\nISSUES FOR PRIORITIZED:" if debug
      pp issues_for_prioritized if debug

      current_iteration = api.iterations(
        group_full_path:,
        iterations_cadence_gid:,
        first: 1,
        state: "current"
      ).first.deep_symbolize_keys

      puts "\n\n\nCURRENT ITERATION:" if debug
      pp current_iteration if debug

      closed_issues_for_current_iteration =
        api.closed_issues_for_current_iteration(
          group_full_path:,
          current_iteration_gid: current_iteration.fetch(:id)
        )

      puts "\n\n\nCLOSED ISSUES FOR CURRENT_ITERATION:" if debug
      pp closed_issues_for_current_iteration if debug

      report_data = {
        board_web_url:,
        closed_iterations:,
        issues_for_unprioritized:,
        issues_for_prioritized:,
        closed_issues_for_current_iteration:,
        current_iteration:
      }.deep_symbolize_keys.to_h

      puts "\n\n\nCONSOLIDATED REPORT DATA:" if debug
      pp report_data if debug

      report_data
    end
  end
end

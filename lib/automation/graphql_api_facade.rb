# frozen_string_literal: true

require "graphql/client"
require "graphql/client/http"

# Prints a velocity-based planning report for the Remote Development team's iteration cadence
module Automation
  # A facade for the GitLab GraphQL API
  # noinspection RubyNilAnalysis,RubyResolve
  class GraphqlApiFacade
    def closed_issues_for_current_iteration(group_full_path:, current_iteration_gid:)
      query_variables = {
        groupFullPath: group_full_path,
        currentIterationId: current_iteration_gid,
      }
      result = Client.query(ClosedIssuesForCurrentIteration, variables: query_variables)

      # noinspection RubyResolve
      unless result.errors.messages.empty?
        pp result.errors
        raise result.errors.messages.to_s
      end

      result.data.group.issues.nodes.map(&:to_h)
    end

    def iterations(group_full_path:, iterations_cadence_gid:, first:, state:)
      query_variables = {
        groupFullPath: group_full_path,
        iterationsCadenceId: iterations_cadence_gid,
        iterationsReportFullPath: group_full_path,
        first:,
        state:,
      }
      result = Client.query(IterationsForGroup, variables: query_variables)

      # noinspection RubyResolve
      unless result.errors.messages.empty?
        pp result.errors
        raise result.errors.messages.to_s
      end

      result.data.group.iterations.nodes.map(&:to_h)
    end

    def lists_for_board(group_full_path:, iteration_planning_board_gid:)
      query_variables = {
        groupFullPath: group_full_path,
        iterationPlanningBoardId: iteration_planning_board_gid,

      }
      result = Client.query(ListsForBoard, variables: query_variables)

      unless result.errors.messages.empty?
        pp result.errors
        raise result.errors.messages.to_s
      end

      result.data.group.board
    end

    def issues_for_board_list(board_list_gid:, label_names:)
      query_variables = {
        boardListId: board_list_gid,
        labelNames: label_names,

      }
      result = Client.query(IssuesForBoardList, variables: query_variables)

      unless result.errors.messages.empty?
        pp result.errors
        raise result.errors.messages.to_s
      end

      # noinspection RubyResolve
      result.data.board_list.issues.nodes.map(&:to_h)
    end

    def self.test_connection_and_token
      uri = parse_api_uri
      # noinspection RubyMismatchedArgumentType
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true if uri.scheme == "https"
      request = Net::HTTP::Post.new(uri.request_uri)
      request["Authorization"] = "Bearer #{ApiToken}"
      request.body = { query: "{ __schema { types { name } } }" }.to_json
      response = http.request(request)

      raise "Failed to connect: #{ApiUri} #{response.code} #{response.message}" unless response.is_a?(Net::HTTPSuccess)
    end

    def self.parse_api_uri
      URI.parse(ApiUri).tap do |uri|
        raise TypeError, "Expected HTTP or HTTPS URI" unless uri.is_a?(URI::HTTP)
      end
    end

    private

    def pagination_limit
      10_000
    end

    # These have to be read at the class level because the idioms for the GraphQL client library usage use constants.
    ApiToken = ENV.fetch("API_TOKEN")
    ApiUri = ENV.fetch("API_URI", "https://gitlab.com/api/graphql")

    HTTP = GraphQL::Client::HTTP.new(ApiUri) do
      # noinspection RbsMissingTypeSignature -- RubyMine incorrectly thinks this is a method on this class, not the lib
      def headers(_context)
        { Authorization: "Bearer #{ApiToken}" }
      end
    end

    test_connection_and_token

    Schema = GraphQL::Client.load_schema(HTTP)

    Client = GraphQL::Client.new(schema: Schema, execute: HTTP)

    ClosedIssuesForCurrentIteration = Client.parse <<~GRAPHQL
      query (
        $groupFullPath: ID!,
        $currentIterationId: [ID],
      ) {
        group(fullPath: $groupFullPath) {
          issues(
            iterationId: $currentIterationId,
            state: closed
            sort: CLOSED_AT_ASC
          ) {
            nodes {
              id
              iid
              state
              weight
              title
              webUrl
              closedAt
            }
          }
        }
      }
    GRAPHQL

    IterationsForGroup = Client.parse <<~GRAPHQL
      query (
        $groupFullPath: ID!,
        $iterationsReportFullPath: String
        $iterationsCadenceId: IterationsCadenceID!,
        $first: Int,
        $state: IterationState
      ) {
        group(fullPath: $groupFullPath) {
          iterations(
            iterationCadenceIds: [$iterationsCadenceId],
            first: $first,
            state: $state
            sort: CADENCE_AND_DUE_DATE_DESC
          ) {
            nodes {
              id
              iid
              title
              description
              startDate
              dueDate
              sequence
              state
              webUrl
              report(fullPath: $iterationsReportFullPath) {
                error {
                  code
                  message
                }
                burnupTimeSeries {
                  date
                  completedCount
                  completedWeight
                  scopeCount
                  scopeWeight
                }
                stats {
                  complete {
                    count
                    weight
                  }
                }
              }
            }
          }
        }
      }
    GRAPHQL

    ListsForBoard = Client.parse <<~GRAPHQL
      query (
        $groupFullPath: ID!,
        $iterationPlanningBoardId: BoardID!,
      ) {
        group(fullPath: $groupFullPath) {
          board(id: $iterationPlanningBoardId) {
            id
            name
            webUrl
            lists {
              nodes {
                id
                title
                listType
                position
                iteration {
                  id
                  state
                }
              }
            }
          }
        }
      }
    GRAPHQL

    IssuesForBoardList = Client.parse <<~GRAPHQL
      query (
        $boardListId: ListID!,
        $labelNames: [String]
      ) {
        boardList(
          id: $boardListId,
          issueFilters: {
            labelName: $labelNames
          },
        ) {
          id
          issues(
            filters: {
              labelName: $labelNames
            },
          ) {
            nodes {
              id
              iid
              state
              relativePosition
              weight
              title
              webUrl
            }
          }
        }
      }
    GRAPHQL
  end
end

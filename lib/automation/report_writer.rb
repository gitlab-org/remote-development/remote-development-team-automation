# frozen_string_literal: true

module Automation
  class ReportWriter
    def write_report(report_data:)
      closed_iteration_summaries, velocity = calculate_velocity_and_closed_iteration_summaries(report_data)

      # TODO: extract and DRY up future iteration logic

      # Assumes 1 week iteration length
      future_iteration_start_date = Date.parse(closed_iteration_summaries.first.fetch(:startDate)) + 7
      future_iteration_due_date = Date.parse(closed_iteration_summaries.first.fetch(:dueDate)) + 7

      future_iterations = Array.new # standard:disable Style/EmptyLiteral -- necessary to make `steep check` happy
      iteration_issues = Array.new # standard:disable Style/EmptyLiteral -- necessary to make `steep check` happy
      iteration_weight = 0
      report_data.fetch(:prioritized_issues).each do |issue|
        issue_weight = get_issue_weight(issue)

        # issue_weight may still be nil...
        if issue_weight && iteration_weight + issue_weight > velocity
          future_iterations << {
            startDate: future_iteration_start_date.strftime("%Y-%m-%d"),
            dueDate: future_iteration_due_date.strftime("%Y-%m-%d"),
            issues: iteration_issues,
            totalWeight: iteration_weight
          }
          future_iteration_start_date += 7
          future_iteration_due_date += 7
          iteration_issues = []
          iteration_weight = 0
        end

        iteration_issues << issue
        iteration_weight += issue_weight if issue_weight
      end

      # Add the last iteration if it contains any issues
      if iteration_issues.any?
        future_iterations << {
          startDate: future_iteration_start_date.strftime("%Y-%m-%d"),
          dueDate: future_iteration_due_date.strftime("%Y-%m-%d"),
          issues: iteration_issues,
          totalWeight: iteration_weight
        }
      end

      report = report_template(
        board_web_url: report_data.fetch(:board_web_url),
        velocity:,
        closed_iteration_summaries:,
        future_iterations:,
        unprioritized_issues: report_data.fetch(:unprioritized_issues),
        current_iteration: report_data.fetch(:current_iteration),
        category_name: report_data.fetch(:category_name),
        report_name_prefix: report_data.fetch(:report_name_prefix)
      )
      puts report
    end

    private

    def get_issue_weight(issue)
      # NOTE: This method is necessary to get RBS to recognize the return type as Integer
      issue.fetch(:weight).to_i
    end

    def report_template(
      board_web_url:,
      velocity:,
      closed_iteration_summaries:,
      future_iterations:,
      unprioritized_issues:,
      current_iteration:,
      category_name:,
      report_name_prefix:
    )
      <<~MARKDOWN
        # #{category_name} Velocity-Based Iteration Planning Report


        ---

        1. [Past Iterations](#past-iterations)
        1. [Prioritized Issues for Current and Future Iterations](#prioritized-issues-for-current-and-future-iterations)
        1. [Unprioritized Issues](#unprioritized-issues)

        *Additional Links*:
        _[Remote Development Group Planning Process](https://handbook.gitlab.com/handbook/engineering/development/dev/create/remote-development/#-planning-process) •
        [#{category_name} Iteration Planning Board](#{board_web_url}) •
        [GitLab Velocity Board Extension](https://gitlab.com/cwoolley-gitlab/gl-velocity-board-extension)_

        _Generated at #{Time.now.strftime("%Y-%m-%d %H:%M:%S:%z")} • [Latest version](https://gitlab-org.gitlab.io/remote-development/remote-development-team-automation/#{report_name_prefix}-latest.html)_

        ---

        ## Past Iterations:

        ### Current Velocity is #{velocity}, calculated for last #{closed_iteration_summaries.size} iterations

        #{closed_iteration_summaries_table(closed_iteration_summaries:)}

        ## Prioritized Issues for Current and Future Iterations:

        (_Note: These future iterations are dynamically calculated based on velocity, but they all map to the
        [single fixed current iteration in standard GitLab](#{current_iteration.fetch(:webUrl)}).
        See [the reasons for this](https://gitlab.com/cwoolley-gitlab/gl-velocity-board-extension#why-doesnt-standard-gitlab-support-this)_)

        #{future_iteration_sections(future_iterations:)}

        ## Unprioritized Issues:

        #{unprioritized_issues_section(unprioritized_issues:)}
      MARKDOWN
    end

    def closed_iteration_summaries_table(closed_iteration_summaries:)
      rows = closed_iteration_summaries.reverse.map do |s|
        "| [#{s.fetch(:startDate)} - #{s.fetch(:dueDate)}](#{s.fetch(:webUrl)}) " \
          "| #{s.fetch(:weight)} | #{s.fetch(:count)} |"
      end.join("\n")

      <<~MARKDOWN
        | Past Iteration | Points | Issue Count |
        | - | - | - |
        #{rows}
      MARKDOWN
    end

    def unprioritized_issues_section(unprioritized_issues:)
      rows = unprioritized_issues.map do |issue|
        "| [#{issue.fetch(:title)}](#{issue.fetch(:webUrl)}) |"
      end.join("\n")

      <<~MARKDOWN
        | Issue Link |
        | - |
        #{rows}

      MARKDOWN
    end

    def future_iteration_sections(future_iterations:)
      future_iterations.map do |future_iteration|
        issue_count = future_iteration.fetch(:issues).size
        closed_issue_count = 0
        rows = future_iteration.fetch(:issues).map do |i|
          closed_text =
            if i.fetch(:state) == "closed"
              closed_issue_count += 1
              " (Closed)"
            else
              ""
            end
          "| #{i.fetch(:weight)} | [#{i.fetch(:title)}](#{i.fetch(:webUrl)})#{closed_text} |"
        end.join("\n")

        iteration_text = "#{future_iteration.fetch(:startDate)} - #{future_iteration.fetch(:dueDate)}"
        header_line = "### #{iteration_text}: #{future_iteration.fetch(:totalWeight)} points, #{issue_count} issues"
        header_line += " (#{closed_issue_count} closed)" if closed_issue_count.positive?
        <<~MARKDOWN
          #{header_line}

          | Weight | Issue Link |
          | - | - |
          #{rows}

        MARKDOWN
      end.join("\n")
    end

    def calculate_velocity_and_closed_iteration_summaries(report_data)
      closed_iterations = report_data.fetch(:closed_iterations)

      completed_iterations_summary_data = closed_iterations.each_with_object(
        {
          completed_weights: 0,
          closed_iteration_summaries: Array.new,  # standard:disable Style/EmptyLiteral -- necessary to make `steep check` happy
        }
      ) do |iteration, acc|
        stats_for_completed = iteration.fetch(:report).fetch(:stats).fetch(:complete)
        acc[:completed_weights] += stats_for_completed.fetch(:weight)
        acc[:closed_iteration_summaries] << {
          startDate: iteration.fetch(:startDate),
          dueDate: iteration.fetch(:dueDate),
          webUrl: iteration.fetch(:webUrl),
        }.merge(stats_for_completed)
      end

      actual_velocity = completed_iterations_summary_data.fetch(:completed_weights).to_f / closed_iterations.size
      velocity = actual_velocity.ceil
      closed_iteration_summaries = completed_iterations_summary_data.fetch(:closed_iteration_summaries)
      [closed_iteration_summaries, velocity]
    end
  end
end
